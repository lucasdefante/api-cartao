package br.com.itau.cartao.controllers;

import br.com.itau.cartao.DTOs.CadastrarCartaoDTO;
import br.com.itau.cartao.DTOs.CartaoMapper;
import br.com.itau.cartao.DTOs.ConsultarCartaoDTO;
import br.com.itau.cartao.DTOs.GetCartaoDTO;
import br.com.itau.cartao.clients.ClienteClient;
import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.models.Cliente;
import br.com.itau.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ConsultarCartaoDTO criarCartao(@RequestBody @Valid CadastrarCartaoDTO cadastrarCartaoDTO){
        Cliente cliente = clienteClient.consultarClientePorId(cadastrarCartaoDTO.getClienteId());
        System.out.println(cliente);
        try {
            Cartao cartao = cartaoMapper.toCartao(cadastrarCartaoDTO);
            Cartao cartaoRetorno = cartaoService.criarCartao(cartao);
            return cartaoMapper.toConsultarCartaoDTO(cartaoRetorno);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("{cartaoId}/pagamento/{pagamentoId}")
    public List<Integer> adicionarPagamentoId(@PathVariable(name = "cartaoId") int cartaoId,
                                              @PathVariable(name = "pagamentoId") int pagamentoId) {
        try {
            Cartao cartao = cartaoService.adicionarPagamentoId(cartaoId, pagamentoId);
            return cartao.getPagamentosId();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public ConsultarCartaoDTO ativarOuDesativarCartao(@RequestBody Map<String, Boolean> map, @PathVariable(name = "numero") String numero){
        try {
            if (map.containsKey("ativo")){
                Cartao cartao = cartaoService.atualizarCartao(map.get("ativo"), numero);
                return cartaoMapper.toConsultarCartaoDTO(cartao);

            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Campo inserido inesperado");
            }
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public GetCartaoDTO consultarCartao(@PathVariable(name = "numero") String numero){
        try {
            Cartao cartao = cartaoService.consultarCartaoPorNumero(numero);
            return cartaoMapper.toGetCartaoDTO(cartao);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/id/{id}")
    public Cartao getCartaoById(@PathVariable(name = "id") int id){
        try {
            return cartaoService.consultarCartaoPorId(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
