package br.com.itau.cartao.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String numero;

    private int clienteId;

    @ElementCollection(targetClass = Integer.class)
    private List<Integer> pagamentosId;

    @ElementCollection(targetClass = Integer.class)
    private List<Integer> faturasId;

    private boolean ativo;

    public Cartao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public List<Integer> getPagamentosId() {
        return pagamentosId;
    }

    public void setPagamentosId(List<Integer> pagamentosId) {
        this.pagamentosId = pagamentosId;
    }

    public void adicionarPagamento(int pagamento) {
        this.pagamentosId.add(pagamento);
    }

    public List<Integer> getFaturasId() {
        return faturasId;
    }

    public void setFaturasId(List<Integer> faturasId) {
        this.faturasId = faturasId;
    }
}
