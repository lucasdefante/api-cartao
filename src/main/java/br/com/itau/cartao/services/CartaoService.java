package br.com.itau.cartao.services;

import br.com.itau.cartao.DTOs.CadastrarCartaoDTO;
import br.com.itau.cartao.DTOs.ConsultarCartaoDTO;
import br.com.itau.cartao.DTOs.GetCartaoDTO;
import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;


    public Cartao criarCartao(Cartao cartao) {
        try {
            return cartaoRepository.save(cartao);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Número de cartão solicitado não está disponível.");
        }
    }

    public Cartao consultarCartaoPorNumero(String numero) {
        Optional<Cartao> optionalCartaoDB = cartaoRepository.findByNumero(numero);

        if (optionalCartaoDB.isPresent()) {
            Cartao cartaoDB = optionalCartaoDB.get();
            return cartaoDB;
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Cartao consultarCartaoPorId(int cartaoId) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(cartaoId);
        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Cartao adicionarPagamentoId(int cartaoId, int pagamentoId) {
        Cartao cartao = consultarCartaoPorId(cartaoId);
        List<Integer> pagamentosId = cartao.getPagamentosId();
        pagamentosId.add(pagamentoId);
        cartao.setPagamentosId(pagamentosId);
        Cartao cartaoDB = cartaoRepository.save(cartao);
        return cartaoDB;
    }

    public Cartao atualizarCartao(boolean ativo, String numero) {
        Cartao cartao = consultarCartaoPorNumero(numero);
        cartao.setAtivo(ativo);
        return cartaoRepository.save(cartao);
    }
}
