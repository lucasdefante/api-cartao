package br.com.itau.cartao.DTOs;

import br.com.itau.cartao.models.Cartao;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class CartaoMapper {

    public Cartao toCartao(CadastrarCartaoDTO cadastrarCartaoDTO) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(false);
        cartao.setNumero(cadastrarCartaoDTO.getNumero());
        cartao.setClienteId(cadastrarCartaoDTO.getClienteId());
        cartao.setPagamentosId(new ArrayList<Integer>());
        cartao.setFaturasId(new ArrayList<Integer>());
        return cartao;
    }

    public Cartao toCartao(ConsultarCartaoDTO consultarCartaoDTO) {
        Cartao cartao = new Cartao();
        cartao.setId(consultarCartaoDTO.getId());
        cartao.setAtivo(consultarCartaoDTO.isAtivo());
        cartao.setNumero(consultarCartaoDTO.getNumero());
        cartao.setClienteId(consultarCartaoDTO.getClienteId());
        return cartao;
    }

    public Cartao toCartao(GetCartaoDTO getCartaoDTO) {
        Cartao cartao = new Cartao();
        cartao.setId(getCartaoDTO.getId());
        cartao.setNumero(getCartaoDTO.getNumero());
        cartao.setClienteId(getCartaoDTO.getClienteId());
        return cartao;
    }

    public CadastrarCartaoDTO toCadastrarCartaoDTO(Cartao cartao) {
        CadastrarCartaoDTO cadastrarCartaoDTO = new CadastrarCartaoDTO();
        cadastrarCartaoDTO.setClienteId(cartao.getClienteId());
        cadastrarCartaoDTO.setNumero(cartao.getNumero());
        return cadastrarCartaoDTO;
    }

    public ConsultarCartaoDTO toConsultarCartaoDTO(Cartao cartao) {
        ConsultarCartaoDTO consultarCartaoDTO = new ConsultarCartaoDTO();
        consultarCartaoDTO.setId(cartao.getId());
        consultarCartaoDTO.setAtivo(cartao.isAtivo());
        consultarCartaoDTO.setNumero(cartao.getNumero());
        consultarCartaoDTO.setClienteId(cartao.getClienteId());
        return consultarCartaoDTO;
    }

    public GetCartaoDTO toGetCartaoDTO(Cartao cartao) {
        GetCartaoDTO getCartaoDTO = new GetCartaoDTO();
        getCartaoDTO.setId(cartao.getId());
        getCartaoDTO.setNumero(cartao.getNumero());
        getCartaoDTO.setClienteId(cartao.getClienteId());
        return getCartaoDTO;
    }
}
