package br.com.itau.cartao.DTOs;

import br.com.itau.cartao.models.Cartao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

public class CadastrarCartaoDTO {

    @NotNull(message = "Número do cartão não pode ser nulo")
    @NotBlank(message = "Número do cartão não pode ser em branco")
    private String numero;

    @NotNull(message = "Id do cliente não pode ser nulo")
    private int clienteId;

    public CadastrarCartaoDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

}
