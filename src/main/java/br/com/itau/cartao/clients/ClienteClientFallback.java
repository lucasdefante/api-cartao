package br.com.itau.cartao.clients;

import br.com.itau.cartao.models.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient{

    @Override
    public Cliente consultarClientePorId(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CLIENTE indisponível no momento. Tente novamente mais tarde.");
    }
}
